This public repository consists of a project BookList-RazorPages developed using ASP.NET Core Razor Pages. 
Using this web application we can be able to perform CRUD operations on Books using book name and author name.

This repository is licensed under MIT license.
The DOTNET official repository is using MIT license and this project is developed using ASP.NET Core Razor Pages
and it is free of cost and flexible.

You are required to have Visual studio 2019 installed in your computer to run the application and some tools
Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation (Option but recommended)
Microsoft.EntityFrameworkCore(required)
Microsoft.EntityFrameworkCore.SqlServer(required)
Microsoft.EntityFrameworkCore.Tools(required)
Microsoft.VisualStudio.Web.CodeGeneration.Design(optional but recommended)
You need to have SSMS installed in your computer as well to manipulate the database and for console purposes.

You can clone the repository or download the project. Open the solution file and you are ready to go.

Please contact Vgolla4104@conestogac.on.ca in case of any queries.

Venkatesh Golla